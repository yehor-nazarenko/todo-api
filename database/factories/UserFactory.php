<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\User;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    $arrayTest = ['testdrive', 'testdrivetestdrive', 'testdrivetestdrivetestdrive'];
    static $index = 0;
    return [
        'username' => "testdrive",
        'email' => $arrayTest[$index++] . "@testdrive.testdrive",
        'verified_at' => now(),
        'password' => bcrypt('testdrive'),
        'avatar' => "https://i.pravatar.cc/150?img=" . $rnd = rand(1,69),
        'avatar_full' => "https://i.pravatar.cc/?img=" . $rnd,
    ];
});
