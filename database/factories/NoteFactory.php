<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Note;
use App\Models\User;
use Carbon\Carbon;
use Faker\Generator as Faker;



$factory->define(Note::class, function (Faker $faker) {
    return [
        'author_id' => rand(1, 3),
        'content' => $faker->text(rand(5, 15)),
    ];
});
