<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notes', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('author_id')->unsigned();
            $table->bigInteger('parent_id')->unsigned()->nullable();
            $table->string('content', 512);
            $table->boolean('completed')->default(false);
            $table->timestamps(6);
            $table->foreign('author_id')->references('id')->on('users')->onDelete('CASCADE');
            $table->foreign('parent_id')->references('id')->on('notes')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('notes', function ($table) {
            $table->dropForeign(['author_id']);
            $table->dropColumn(['author_id']);
        });
        Schema::dropIfExists('notes');
    }
}
