<?php

use App\Models\Note;
use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class, 2)->create()->each(function ($user) {
            factory(Note::class, 20)->create(['author_id' => $user->id])->each(function ($note) use ($user){
                factory(Note::class, rand(0,3))->create(['parent_id' => $note->id, 'author_id' => $user->id]);
            });
        });
    }
}
