<?php

use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => ['api', 'jwt'],
], function () {
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('account', 'AuthController@account');
    Route::resource('notes', 'NoteController');
    Route::resource('notes/{id}/subNotes', 'SubNoteController');
});

Route::post('login', 'AuthController@login');
Route::post('register', 'AuthController@register');
