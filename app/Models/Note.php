<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed parent_id
 */
class Note extends Model
{
    protected $fillable = [
        'content',
        'completed'
    ];

    protected $dateFormat = 'Y-m-d H:i:s.u';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(Note::class, 'parent_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany(Note::class, 'parent_id');
    }

    public function scopeChildrenDesc($query)
    {
        return $query->whereNull('parent_id')->with('children')->latest();
    }

    public function scopeFindOrFailNote($query, $noteId)
    {
        return $query->whereNull('parent_id')->findOrFail($noteId);
    }

    public function scopeFindOrFailSubNote($query, $noteId, $subNoteId)
    {
        return $query->where('parent_id', $noteId)->findOrFail($subNoteId);
    }
}
