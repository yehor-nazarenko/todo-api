<?php

namespace App\Http\Controllers;

use App\Http\Requests\NoteRequest;
use App\Models\Note;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

class NoteController extends Controller
{
    /**
     * @param Request $request
     * @return mixed
     */
    public function index(Request $request)
    {
        $user = $request->user();
        return $user->notes()->childrenDesc()->paginate(10);
    }

    /**
     * @param NoteRequest $request
     * @return Note|Application|ResponseFactory|Response
     */
    public function store(NoteRequest $request)
    {
        $user = $request->user();
        $note = new Note($request->all());
        return $user->notes()->save($note)->load('children');
    }

    /**
     * @param Request $request
     * @param $id
     * @return Application|ResponseFactory|Response
     */
    public function show(Request $request, $id)
    {
        $user = $request->user();
        $note = $user->notes()->findOrFail($id);
        return $note->load('children');
    }

    /**
     * @param NoteRequest $request
     * @param $id
     * @return Application|ResponseFactory|Response
     */
    public function update(NoteRequest $request, $id)
    {
        $user = $request->user();
        $note = $user->notes()->findOrFailNote($id);;
        $note->update($request->all());
        return $note->load('children');
    }

    /**
     * @param Request $request
     * @param $id
     * @return Application|ResponseFactory|Response
     */
    public function destroy(Request $request, $id)
    {
        $user = $request->user();
        $note = $user->notes()->findOrFailNote($id);
        return $note->delete();
    }
}
