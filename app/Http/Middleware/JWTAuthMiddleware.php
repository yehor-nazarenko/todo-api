<?php

namespace App\Http\Middleware;

use Closure;

class JWTAuthMiddleware
{
    /**
     * @param $request
     * @param Closure $next
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response|mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();
        if(!$user){
            return response(null, 401);
        }
        return $next($request);
    }
}
